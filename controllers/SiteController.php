<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionConsulta1a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find()-> select("edad")->distinct(),
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['edad'],
          "titulo" => "Consulta 1 con ActiveRecord",
          "enunciado" => "Listar llas edades de los ciclistas (sin repetidos)",
          "sql" => "SELECT DISTINCT edad FROM ciclistas",
        ]);
    }
    
    public function actionConsulta1d() {
        $total = Yii::$app -> db
                -> createCommand('select count(distinct edad) from ciclista')
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => 'SELECT DISTINCT edad FROM ciclista',
          'totalCount' => $total,
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['edad'],
          "titulo" => "Consulta 1 con DAO",
          "enunciado" => "Listar las edades de los ciclistas (sin repetidos)",
          "sql" => "SELECT DISTINCT edad FROM ciclistas",
        ]);
    }
    
    public function actionConsulta2a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> where("nomequipo = 'Artiach'") ->select("edad")->distinct(),
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['edad'],
          "titulo" => "Consulta 2 con ActiveRecord",
          "enunciado" => "Listar las edades de los ciclistas de Artiach",
          "sql" => "SELECT DISTINCT edad FROM ciclista",
        ]);
    }
    
    public function actionConsulta2d() {
        $total = Yii::$app -> db
                -> createCommand("select count(distinct edad) from ciclista where nomequipo = 'Artiach'")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT edad FROM ciclista WHERE nomequipo = 'Artiach'",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['edad'],
          "titulo" => "Consulta 2 con DAO",
          "enunciado" => "Listar las edades de los ciclistas de Artiach",
          "sql" => "SELECT DISTINCT edad FROM ciclistas WHERE nomequipo = 'Artiach'",
        ]);
    }
    
    public function actionConsulta3a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() ->select("edad")->distinct() 
                -> where("nomequipo = 'Artiach'") ->orWhere("nomequipo = 'Amore Vita'"),
          'pagination' => ['pageSize' =>4],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['edad'],
          "titulo" => "Consulta 3 con ActiveRecord",
          "enunciado" => "Listar las edades de los ciclistas de Artiach o de Amore Vita",
          "sql" => "SELECT DISTINCT edad FROM ciclista WHERE nomequipo='Artiach' OR nomequipo='Amore Vita'",
        ]);
    }
    
    public function actionConsulta3d() {
        $total = Yii::$app -> db
                -> createCommand("select count(distinct edad) from ciclista where nomequipo "
                        . "= 'Artiach' or nomequipo = 'Amore Vita' ")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT edad FROM ciclista WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>4],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['edad'],
          "titulo" => "Consulta 3 con DAO",
          "enunciado" => "Listar las edades de los ciclistas de Artiach o de Amore Vita",
          "sql" => "SELECT DISTINCT edad FROM ciclistas WHERE nomequipo = 'Artiach' OR nomequipo = 'Amore Vita'",
        ]);
    }
    
    public function actionConsulta4a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() ->select("dorsal")->where("edad < 25")-> orWhere("edad > 30"),
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 4 con ActiveRecord",
          "enunciado" => "Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
          "sql" => "SELECT dorsal FROM ciclista WHERE edad<25 OR edad>30",
        ]);
    }
    
    public function actionConsulta4d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT count(dorsal) FROM ciclista WHERE edad<25 OR edad>30")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT dorsal FROM ciclista WHERE edad<25 OR edad>30",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 4 con DAO",
          "enunciado" => "Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
          "sql" => "SELECT dorsal FROM ciclista WHERE edad<25 OR edad>30",
        ]);
    }
    
    public function actionConsulta5a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() ->select("dorsal")->where("edad > 28")
                -> andWhere("edad < 32") -> andWhere("nomequipo = 'Banesto'"),
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 5 con ActiveRecord",
          "enunciado" => "Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
          "sql" => "SELECT dorsal FROM ciclista WHERE edad>28 AND edad<32 AND nomequipo='Banesto'",
        ]);
    }
    
     public function actionConsulta5d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT count(dorsal) FROM ciclista WHERE edad>28 AND edad<32 AND nomequipo='Banesto'")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT dorsal FROM ciclista WHERE edad>28 AND edad<32 AND nomequipo='Banesto'",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 5 con DAO",
          "enunciado" => "Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto",
          "sql" => "SELECT dorsal FROM ciclista WHERE edad>28 AND edad<32 AND nomequipo='Banesto'",
        ]);
    }
    
    public function actionConsulta6a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> select("nombre") -> distinct() -> where("CHAR_LENGTH(nombre)>8"),
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['nombre'],
          "titulo" => "Consulta 6 con ActiveRecord",
          "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
          "sql" => "SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
        ]);
    }
    
    public function actionConsulta6d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT count(distinct nombre) FROM ciclista WHERE CHAR_LENGTH(nombre)>8")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['nombre'],
          "titulo" => "Consulta 6 con DAO",
          "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
          "sql" => "SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
        ]);
    }
    
    public function actionConsulta7a() {
        //Para añadir un alias a un campo, vamos al archivo del modelo y creamos y ponemos public $elalias
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Ciclista::find() -> select("dorsal, UPPER(nombre) as Nombre_Mayuscula") ->distinct(),
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
            //Asi podemos meter directamente el alias como si fuera un campo.
          "campos" => ['dorsal','Nombre_Mayuscula'],
          "titulo" => "Consulta 7 con ActiveRecord",
          "enunciado" => "Lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
          "sql" => "SELECT DISTINCT UPPER(nombre) AS 'Nombre_Mayuscula', dorsal FROM ciclista",
        ]);
    }
    
    public function actionConsulta7d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT COUNT(dorsal) FROM ciclista")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT DISTINCT UPPER(nombre) AS 'Nombre_Mayuscula', dorsal FROM ciclista",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal','Nombre_Mayuscula'],
          "titulo" => "Consulta 7 con DAO",
          "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
          "sql" => "SELECT DISTINCT UPPER(nombre) AS 'Nombre_Mayuscula', dorsal FROM ciclista",
        ]);
    }
    
    public function actionConsulta8a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Lleva::find() -> select("dorsal") ->where("código = 'MGE'") ->distinct(),
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 8 con ActiveRecord",
          "enunciado" => "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
          "sql" => "SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'",
        ]);
    }
    
    public function actionConsulta8d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT COUNT(DISTINCT dorsal) FROM lleva WHERE código = 'MGE'")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 8 con DAO",
          "enunciado" => "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
          "sql" => "SELECT DISTINCT dorsal FROM lleva WHERE código = 'MGE'",
        ]);
    }
    
    public function actionConsulta9a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Puerto::find() -> select("nompuerto") ->where("altura > 1500"),
          'pagination' => ['pageSize' =>5],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['nompuerto'],
          "titulo" => "Consulta 9 con ActiveRecord",
          "enunciado" => "Listar el nombre de los puertos cuya altura sea mayor de 1500",
          "sql" => "SELECT nompuerto FROM puerto WHERE altura>1500",
        ]);
    }
    
    public function actionConsulta9d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT COUNT(nompuerto) FROM puerto WHERE altura>1500")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT nompuerto FROM puerto WHERE altura>1500",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['nompuerto'],
          "titulo" => "Consulta 9 con DAO",
          "enunciado" => "Listar el nombre de los puertos cuya altura sea mayor de 1500",
          "sql" => "SELECT nompuerto FROM puerto WHERE altura>1500",
        ]);
    }
    
    public function actionConsulta10a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Puerto::find() -> select("nompuerto") -> where("pendiente > 8") 
                -> orWhere("altura >1800") -> andWhere("altura < 3000"),
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['nompuerto'],
          "titulo" => "Consulta 10 con ActiveRecord",
          "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
          "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura>1800 AND altura<3000",
        ]);
    }
    
    public function actionConsulta10d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT COUNT(DISTINCT dorsal) FROM puerto WHERE pendiente>8 OR altura>1800 AND altura<3000")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura>1800 AND altura<3000",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 10 con DAO",
          "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
          "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 OR altura>1800 AND altura<3000",
        ]);
    }
    
    public function actionConsulta11a() {
        $dataProvider = new ActiveDataProvider ([
          'query' => \app\models\Puerto::find() -> select("nompuerto") -> where("pendiente > 8") 
                -> andWhere("altura >1800") -> andWhere("altura < 3000"),
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 11 con ActiveRecord",
          "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
          "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura>1800 AND altura<3000",
        ]);
    }
    
     public function actionConsulta11d() {
        $total = Yii::$app -> db
                -> createCommand("SELECT COUNT(DISTINCT dorsal) FROM puerto WHERE pendiente>8 AND altura>1800 AND altura<3000")
                -> queryScalar();
        $dataProvider = new SqlDataProvider ([
          'sql' => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura>1800 AND altura<3000",
          'totalCount' => $total,
          'pagination' => ['pageSize' =>6],
        ]);
        
        return $this->render("resultados",[
          "resultados" => $dataProvider,
          "campos" => ['dorsal'],
          "titulo" => "Consulta 10 con DAO",
          "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
          "sql" => "SELECT DISTINCT dorsal FROM puerto WHERE pendiente>8 AND altura>1800 AND altura<3000",
        ]);
    }
}
