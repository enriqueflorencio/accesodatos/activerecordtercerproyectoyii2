<?php

use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de selección 1</h1>
    </div>
    <div class="body-content">
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta I</h3>
                    <p>
                    <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <?= Html::a('Active Record', ['site/consulta1a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta1d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta II</h3>
                    <p>
                         <p>Listar las edades de los ciclistas de Artiach</p>
                        <?= Html::a('Active Record', ['site/consulta2a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta2d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta III</h3>
                    <p>
                    <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <?= Html::a('Active Record', ['site/consulta3a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta3d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta IV</h3>
                    <p>
                    <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <?= Html::a('Active Record', ['site/consulta4a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta4d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta V</h3>
                    <p>
                    <p>Listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto</p>
                        <?= Html::a('Active Record', ['site/consulta5a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta5d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta VI</h3>
                    <p>
                    <p>Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8</p>
                        <?= Html::a('Active Record', ['site/consulta6a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta6d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta VII</h3>
                    <p>
                    <p>Lístame el nombre y el dorsal de todos los ciclistas, poniendo los nombre en mayúsculas</p>
                        <?= Html::a('Active Record', ['site/consulta7a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta7d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta VIII</h3>
                    <p>
                    <p>Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa</p>
                        <?= Html::a('Active Record', ['site/consulta8a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta8d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta IX</h3>
                    <p>
                    <p>Listar el nombre de los puertos cuya altura sea mayor de 1500 </p>
                        <?= Html::a('Active Record', ['site/consulta9a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta9d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta X</h3>
                    <p>
                    <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000 </p>
                        <?= Html::a('Active Record', ['site/consulta10a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta10d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
        <div class ="col-sm-6 col-md-4">
            <div class ="thumbnail">
                <div class="caption">
                    <h3>Consulta XI</h3>
                    <p>
                    <p>Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000 </p>
                        <?= Html::a('Active Record', ['site/consulta11a'], ['class' => 'btn btn-primary'])?>
                        <?= Html::a('DAO', ['site/consulta11d'], ['class' => 'btn btn-default'])?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
